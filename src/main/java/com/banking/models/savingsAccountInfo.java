package com.banking.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="SAVINGS_ACCTS")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class savingsAccountInfo {


    @Column(name="CLIENT_ID")
    @NonNull
    private int clientId;

    @Column(name="ROUT_NUM")
    @NonNull
    private long routNum;

    @Column(name="SAV_ACC_NUM",unique = true)
    @NonNull
    private long savAccNum;

    @Column(name="SAV_ACC_BAL")
    @NonNull
    private long savAccBal;

    @Id
    @Column(name="dummy_PK")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int dummy_PK;

}
